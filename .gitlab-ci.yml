image: docker:19.03.12

services:
- docker:19.03.12-dind

include:
  - template: Code-Quality.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml

variables:
  CI_APPLICATION_TAG: $CI_COMMIT_TAG
  DOCKER_TLS_CERTDIR: "/certs"
  LINUX_AMD64_BINARY: "go-take-me-home-linux-amd64"
  OSX_AMD64_BINARY: "go-take-me-home-osx-amd64"
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/go-take-me-home/${CI_COMMIT_TAG}"

stages:
  - build
  - test
  - upload
  - release

build-any:
  stage: build
  except:
    - tags
    - master
  script:
  - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  - docker build --build-arg COMMIT_SHA=${CI_COMMIT_SHA} --build-arg COMMIT_BRANCH=${CI_COMMIT_BRANCH} -t $CI_REGISTRY_IMAGE .
  - docker image ls
  - docker tag $CI_REGISTRY_IMAGE $CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH
  - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH

build-docker-master:
  stage: build
  only:
    refs:
    - master
  script:
  - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  - docker build --build-arg COMMIT_SHA=${CI_COMMIT_SHA} --build-arg COMMIT_BRANCH=${CI_COMMIT_BRANCH} -t $CI_REGISTRY_IMAGE .
  - docker tag $CI_REGISTRY_IMAGE $CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH
  - docker push $CI_REGISTRY_IMAGE:latest
  - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH

build-tags-linux-amd64:
  stage: build
  only:
  - tags
  script:
  - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  - docker build --build-arg COMMIT_SHA=${CI_COMMIT_SHA} --build-arg COMMIT_BRANCH=${CI_COMMIT_BRANCH} --build-arg COMMIT_TAG=${CI_COMMIT_TAG} -t $CI_REGISTRY_IMAGE .
  - docker tag $CI_REGISTRY_IMAGE $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG # We also build, tag, and push the docker image here
  - id=$(docker create $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG)
  - docker cp $id:/go/bin/go-take-me-home ./${LINUX_AMD64_BINARY}
  artifacts:
    paths:
    - ./${LINUX_AMD64_BINARY}

build-tags-osx-amd64:
  stage: build
  variables:
    GOOS: darwin
    GOARCH: amd64
  only:
  - tags
  script:
  - docker build --build-arg COMMIT_SHA=${CI_COMMIT_SHA} --build-arg COMMIT_BRANCH=${CI_COMMIT_BRANCH} --build-arg COMMIT_TAG=${CI_COMMIT_TAG} -t $CI_REGISTRY_IMAGE .
  - docker tag $CI_REGISTRY_IMAGE $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  - id=$(docker create $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG)
  - docker cp $id:/go/bin/go-take-me-home ./${OSX_AMD64_BINARY}
  artifacts:
    paths:
    - ./${OSX_AMD64_BINARY}

upload-binary-tags:
  stage: upload
  image: curlimages/curl:latest
  only:
  - tags
  script:
  - |
    curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ./${LINUX_AMD64_BINARY} ${PACKAGE_REGISTRY_URL}/${LINUX_AMD64_BINARY}
  - |
    curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ./${OSX_AMD64_BINARY} ${PACKAGE_REGISTRY_URL}/${OSX_AMD64_BINARY}

release-tags:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  only:
  - tags
  script:
  - |
    release-cli create --name "Release $CI_COMMIT_TAG" --tag-name $CI_COMMIT_TAG \
      --assets-link "{\"name\":\"${LINUX_AMD64_BINARY}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${LINUX_AMD64_BINARY}\"}" \
      --assets-link "{\"name\":\"${OSX_AMD64_BINARY}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${OSX_AMD64_BINARY}\"}" 

code_quality:
  stage: test
  needs: []
  artifacts:
    paths: [gl-code-quality-report.json]