FROM golang:alpine AS build-env

WORKDIR $GOPATH/src/gitlab.com/cworobetz/go-take-me-home

# Arguments are optional
ARG COMMIT_SHA
ARG COMMIT_TAG
ARG COMMIT_BRANCH

ENV CGO_ENABLED=0

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN go build -ldflags="-X 'main.gitCommit=${COMMIT_SHA:-unknown}' -X 'main.gitTag=${COMMIT_TAG:-unknown}' -X 'main.gitBranch=${COMMIT_BRANCH:-unknown}'" -a -o /go/bin/go-take-me-home

FROM scratch
COPY --from=build-env /go/bin/go-take-me-home /go/bin/go-take-me-home

ENTRYPOINT ["/go/bin/go-take-me-home"]