module gitlab.com/cworobetz/go-take-me-home

go 1.13

require (
	github.com/go-git/go-git/v5 v5.1.0
	github.com/imdario/mergo v0.3.11 // indirect
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
	golang.org/x/net v0.0.0-20200927032502-5d4f70055728 // indirect
	golang.org/x/sys v0.0.0-20200926100807-9d91bd62050c // indirect
)
