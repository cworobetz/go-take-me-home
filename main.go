package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"gitlab.com/cworobetz/go-take-me-home/plumbing"
)

var (
	gitCommit string = ""
	gitTag    string = ""
	gitBranch string = ""
)

func main() {

	var err error

	version := flag.Bool("version", false, "Prints the version of this program then exits")
	repoURL := flag.String("repo", os.Getenv("GTMH_REPO"), "The URL of your dotfile git repository. Takes the value of env var $GTMH_REPO as default")
	pull := flag.Bool("pull", true, "Pull changes and overwrite local files defined in ~/.config/go-take-me-home/includes")
	push := flag.Bool("push", false, "Push changes and overwrite remote files defined in ~/.config/go-take-me-home/includes")
	username := flag.String("username", "git", "The username to authenticate with your git repository.")
	passwordprompt := flag.Bool("password", false, "Whether or not to prompt for password.")

	flag.Parse()

	if *version {
		fmt.Printf("go-take-me-home: Version{gitCommit: %s, gitTag: %s, gitBranch: %s}\n", gitCommit, gitTag, gitBranch)
		os.Exit(0)
	}

	if *repoURL == "" {
		log.Fatalln("Error: repository URL is required. Use -help for more information.")
	}

	if !*push && !*pull {
		log.Fatalf("Error: You must either push or pull")
	}

	password := ""
	if *passwordprompt {
		password = plumbing.ReadPassword()
	}

	if *push {
		*pull = false
	}

	// Perform main action
	if *pull {
		fmt.Println("Specified pull")
		err = plumbing.PullAndCopy(repoURL, username, password)
	}
	if *push {
		fmt.Println("Specified push")
		// Pull latest changes from git
		err = plumbing.Pull(repoURL, username, password)
		if err != nil {
			log.Fatalf("go-take-me-home: %v", err)
		}
		err = plumbing.CopyAndPush(repoURL, username, password)
	}
	if err != nil {
		log.Fatalf("go-take-me-home: %v", err)
	}
}
