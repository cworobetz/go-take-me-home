package plumbing

import (
	"bufio"
	"log"
	"os"
	"os/user"
	"path"
	"path/filepath"
)

// Include is simply a relative path to an included file. It represents a line in the includes file
type Include struct {
	RelativePath string
}

// parseLocalIncludes will return an []Include object with the users's local includes
func parseLocalIncludes() ([]Include, error) {
	usr, _ := user.Current()
	dir := usr.HomeDir

	localpath := path.Join(filepath.FromSlash(dir + "/.config/go-take-me-home/includes"))
	if _, err := os.Stat(localpath); os.IsNotExist(err) { // May error if local includes does not exist
		log.Fatalf("Local includes file (" + localpath + ") does not exist. Please create one and list local files you wish to manage")
		return nil, err
	}

	file, err := os.Open(localpath)
	if err != nil { // May error due to permissions
		return nil, err
	}
	defer file.Close() // file is read only, so OK to defer

	var localIncludes []Include
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		localIncludes = append(localIncludes, Include{RelativePath: scanner.Text()})
	}
	return localIncludes, nil
}

// isIncluded checks whether a file is able to be included, based off the local includes file
func isIncluded(base string, file string) (bool, error) {
	includes, err := parseLocalIncludes()
	if err != nil {
		return false, err
	}

	for _, include := range includes {
		rel, err := filepath.Rel(base, file)
		if err != nil {
			return false, err
		}
		if include.RelativePath == rel {
			return true, nil
		}
	}
	return false, nil
}
