package plumbing

import (
	"fmt"
	"net/url"
	"os/user"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/transport/http"
)

// GIT_REMOTE is which git remote to use when pulling
const gitRemote = "origin"

// PullAndCopy initiates and finishes a pull from a remote repository. It also overwrites any of the user's home dir files defined in ~/.config/go-take-me-home/includes
func PullAndCopy(repoURL *string, username *string, password string) error {

	usr, _ := user.Current()
	homedir := usr.HomeDir

	// Pull latest changes from git
    fmt.Printf("Pulling latest changes from git... ")
	err := pullRemoteToTmp(repoURL, username, password)
	if err != nil && err != git.NoErrAlreadyUpToDate {
		return err
	}
    fmt.Printf("done\n")

	// Copy from the remote repo's tmp path to user's local home dir
	err = copyFiles(getTmpPath(*repoURL), homedir)
	if err != nil {
		return err
	}

	return nil
}

// Pull initiates and finishes a pull from a remote repository. It does not copy the files to the users home directory
func Pull(repoURL *string, username *string, password string) error {

	// Pull latest changes from git
    fmt.Printf("Pulling latest changes from git... ")
	err := pullRemoteToTmp(repoURL, username, password)
	if err != nil && err != git.NoErrAlreadyUpToDate {
        fmt.Printf("error\n")
		return err
	}

    fmt.Printf("done\n")
	return nil
}

// pullRemoteToTmp will clone a git repository to a predetermined tmp directory, based off the repo URL
func pullRemoteToTmp(repoURL *string, username *string, password string) error {

	// Convert into parsable format
	u, err := url.Parse(*repoURL)
	if err != nil {
		return err
	}

	auth := &http.BasicAuth{}
	if *username != "" && password != "" {
		auth = &http.BasicAuth{
			Username: *username,
			Password: password,
		}
	}

	// Grab the temp path
	tmpPath := getTmpPath(u.String())

	// Try to clone
	_, err = git.PlainClone(tmpPath, false, &git.CloneOptions{
		URL:  u.String(),
		Auth: auth,
	})
	if err == git.ErrRepositoryAlreadyExists { // Repository has already been cloned

		// Open the repository, then the worktree, and attempt the pull
		r, err := git.PlainOpen(tmpPath)
		if err != nil {
			return err
		}
		w, err := r.Worktree()
		if err != nil {
			return err
		}
		err = w.Pull(&git.PullOptions{
			RemoteName: "origin",
			Auth:       auth,
		})
		if err != nil {
			return err
		}
	} else if err != nil {
		return err
	}

	fmt.Printf("Finished pull\n")
	return nil
}
