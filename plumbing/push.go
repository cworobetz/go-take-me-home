package plumbing

import (
	"fmt"
	"os"
	"os/user"
	"time"

	"github.com/go-git/go-git/v5/plumbing/object"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/config"
	"github.com/go-git/go-git/v5/plumbing/transport/http"
)

// CopyAndPush copies files from the user's home dir to the tmp repo, and pushes them to git.
func CopyAndPush(repoURL *string, username *string, password string) error {

	usr, _ := user.Current()
	homedir := usr.HomeDir

	// Copy local files present in the user's includes file to the tmp location
	err := copyFiles(homedir, getTmpPath(*repoURL))
	if err != nil {
		return err
	}

	// Push latest changes to git.
	if err = pushTmpToRemote(repoURL, username, password); err != nil {
		return err
	}

	return nil
}

func pushTmpToRemote(repoURL *string, username *string, password string) error {

	auth := &http.BasicAuth{
		Username: *username,
		Password: password,
	}

	tmpPath := getTmpPath(*repoURL)

	// The path must exist first
	if _, err := os.Stat(tmpPath); err != nil {
		return fmt.Errorf("git repository does not exist on local system")
	}

	// Open the working tree
	repo, err := git.PlainOpen(tmpPath)
	if err == git.ErrRepositoryNotExists {
		return fmt.Errorf("repository does not exist at " + tmpPath + ". Please run \"go-take-me-home -repo <repo> -pull\" first to initialize")
	} else if err != nil {
		return err
	}

	worktree, _ := repo.Worktree()
	fmt.Printf("Commiting changes... ")

	// Add any new files
	_, err = worktree.Add(".")
	if err != nil {
		return err
	}

	// Check if it's clean. If so, don't bother commiting. Else, commit.
	status, err := worktree.Status()
	if status.IsClean() {
		fmt.Printf("nothing new to commit to local repository\n")
	} else {
		_, err = worktree.Commit(time.Now().Format(time.RFC3339), &git.CommitOptions{
			All: true,
			Author: &object.Signature{
				Name:  "go-take-me-home",
				Email: "go-take-me-home@localhost",
				When:  time.Now(),
			},
			Committer: nil,
		})
		if err != nil {
			return err
		}
		fmt.Printf("done\n")
	}

	// Push
	fmt.Printf("Pushing changes... ")
	err = repo.Push(&git.PushOptions{
		RemoteName: "origin",
		Auth:       auth,
		RefSpecs: []config.RefSpec{
			config.RefSpec(config.DefaultPushRefSpec),
		},
	})
	if err == git.NoErrAlreadyUpToDate {
		fmt.Printf("already up to date\n")
		return nil
	}
	if err != nil {
		return err
	}
	fmt.Printf("done\n")

	return nil
}
