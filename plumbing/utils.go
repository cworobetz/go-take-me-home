package plumbing

import (
	"encoding/hex"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
	"syscall"

	"golang.org/x/crypto/ssh/terminal"
)

// copyFiles will copy from src to dest, taking into account the includes file
func copyFiles(src string, dest string) error {

	files, err := listFilesToCopy(src)
	if err != nil {
		return err
	}

	for _, file := range files {
		relativePath, err := filepath.Rel(src, file)
		if err != nil {
			return err
		}
		if err := copyFile(filepath.Join(src, relativePath), filepath.Join(dest, relativePath)); err != nil {
			return err
		}
	}
	return nil
}

// copyFile is similar to unix rm. It will overwrite the file at the dst, if it exists.
// This function is long due to all the error checking, can it be shortened?
func copyFile(srcPath string, destPath string) error {
	sourceFileStat, err := os.Stat(srcPath)
	if err != nil {
		return err
	}

	if !sourceFileStat.Mode().IsRegular() {
		err := fmt.Errorf("%s is not a regular file", srcPath)
		return err
	}

	// Remove the existing file at destination, if it exists
	err = os.RemoveAll(destPath)
	if err != nil {
		return err
	}

	source, err := os.Open(srcPath)
	if err != nil {
		return err
	}
	defer source.Close()

	// Make the directory if it doesn't already exist
	if err := os.MkdirAll(filepath.Dir(destPath), 0750); err != nil {
		return err
	}

	destination, err := os.Create(destPath)
	if err != nil {
		return err
	}

	_, err = io.Copy(destination, source)
	if err != nil {
		return err
	}

	err = destination.Close()
	if err != nil {
		return err
	}

	err = source.Close()
	if err != nil {
		return err
	}

	return nil
}

// toRelativePath turns an absolute path to a relative path by removing src
func toRelativePath(src string, file string) string {
	relPath := strings.Replace(file, src, "", 1)
	return relPath
}

// getTmpPath takes the repo URL as an input, and returns the tmp path on the filesystem that will be used to stage dotfiles. It creates the path if it doesn't exist.
func getTmpPath(repoURL string) string {
	hex := hex.EncodeToString([]byte(repoURL))

	path := os.TempDir() + filepath.FromSlash("/go-take-me-home-"+hex[:])

	if _, err := os.Stat(path); os.IsNotExist(err) {
		os.Mkdir(path, 0750)
	}

	return path
}

// listFilesToCopy will return a list of all files from src that should be copied based off the user's includes file
func listFilesToCopy(src string) ([]string, error) {

	var allFiles []string
	var includedFiles []string

	// Get a list of all files at src
	err := filepath.Walk(src, func(path string, info os.FileInfo, err error) error {
		allFiles = append(allFiles, path)
		return nil
	})
	if err != nil {
		return nil, err
	}

	// Filter to only include included files
	allFiles = allFiles[1:]         // Discard the first element, the root path.
	for _, file := range allFiles { // TODO - extract just the filename / directory name to be able to operate just on that
		if result, err := isIncluded(src, file); result == true {
			includedFiles = append(includedFiles, file)
		} else if err != nil {
			return nil, err
		}
	}
	return includedFiles, nil
}

// ReadPassword will silent read a password from Stdin and return it as a string
func ReadPassword() string {
	fmt.Print("Enter password: ")
	bytePassword, err := terminal.ReadPassword(int(syscall.Stdin))
	fmt.Printf("\n")
	if err != nil {
		log.Fatalf("credentials: %v\n", err)
	}
	return string(bytePassword)
}
